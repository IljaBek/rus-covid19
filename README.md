# SARS-COV-2 Case numbers in RUS


## Москва
![plot for 2](./plot_2.png)

## Санкт-Петербург
![plot for 4](./plot_4.png)

## Московская область
![plot for 3](./plot_3.png)

## Нижегородская область
![plot for 9](./plot_9.png)

## Ростовская область
![plot for 60](./plot_60.png)

## Свердловская область
![plot for 5](./plot_5.png)

## Воронежская область
![plot for 13](./plot_13.png)

## Красноярский край
![plot for 14](./plot_14.png)

## Иркутская область
![plot for 62](./plot_62.png)

## Архангельская область
![plot for 26](./plot_26.png)

## Самарская область
![plot for 6](./plot_6.png)

## Челябинская область
![plot for 51](./plot_51.png)

## Саратовская область
![plot for 48](./plot_48.png)

## Волгоградская область
![plot for 42](./plot_42.png)

## Ханты-Мансийский АО
![plot for 34](./plot_34.png)

## Пермский край
![plot for 21](./plot_21.png)

## Ульяновская область
![plot for 50](./plot_50.png)

## Ставропольский край
![plot for 49](./plot_49.png)

## Хабаровский край
![plot for 19](./plot_19.png)

## Мурманская область
![plot for 53](./plot_53.png)

## Алтайский край
![plot for 63](./plot_63.png)

## Республика Карелия
![plot for 78](./plot_78.png)

## Омская область
![plot for 58](./plot_58.png)

## Краснодарский край
![plot for 15](./plot_15.png)

## Вологодская область
![plot for 57](./plot_57.png)

## Пензенская область
![plot for 36](./plot_36.png)

## Приморский край
![plot for 31](./plot_31.png)

## Республика Коми
![plot for 47](./plot_47.png)

## Забайкальский край
![plot for 35](./plot_35.png)

## Ленинградская область
![plot for 22](./plot_22.png)

## Оренбургская область
![plot for 46](./plot_46.png)

## Кировская область
![plot for 8](./plot_8.png)

## Новосибирская область
![plot for 18](./plot_18.png)

## Республика Крым
![plot for 10](./plot_10.png)

## Тверская область
![plot for 23](./plot_23.png)

## Ямало-Ненецкий автономный округ
![plot for 81](./plot_81.png)

## Тульская область
![plot for 17](./plot_17.png)

## Ярославская область
![plot for 38](./plot_38.png)

## Брянская область
![plot for 41](./plot_41.png)

## Республика Бурятия
![plot for 55](./plot_55.png)

## Белгородская область
![plot for 40](./plot_40.png)

## Курская область
![plot for 71](./plot_71.png)

## Псковская область
![plot for 59](./plot_59.png)

## Тюменская область
![plot for 20](./plot_20.png)

## Кемеровская область
![plot for 29](./plot_29.png)

## Республика Башкортостан
![plot for 80](./plot_80.png)

## Республика Саха (Якутия)
![plot for 11](./plot_11.png)

## Ивановская область
![plot for 43](./plot_43.png)

## Орловская область
![plot for 30](./plot_30.png)

## Калужская область
![plot for 27](./plot_27.png)

## Астраханская область
![plot for 67](./plot_67.png)

## Удмуртская Республика
![plot for 33](./plot_33.png)

## Томская область
![plot for 39](./plot_39.png)

## Республика Дагестан
![plot for 56](./plot_56.png)

## Владимирская область
![plot for 73](./plot_73.png)

## Калининградская область
![plot for 7](./plot_7.png)

## Тамбовская область
![plot for 24](./plot_24.png)

## Новгородская область
![plot for 45](./plot_45.png)

## Липецкая область
![plot for 16](./plot_16.png)

## Смоленская область
![plot for 75](./plot_75.png)

## Рязанская область
![plot for 32](./plot_32.png)

## Республика Чувашия
![plot for 54](./plot_54.png)

## Кабардино-Балкарская Республика
![plot for 28](./plot_28.png)

## Сахалинская область
![plot for 66](./plot_66.png)

## Амурская область
![plot for 70](./plot_70.png)

## Республика Хакасия
![plot for 37](./plot_37.png)

## Костромская область
![plot for 61](./plot_61.png)

## Республика Калмыкия
![plot for 65](./plot_65.png)

## Республика Татарстан
![plot for 12](./plot_12.png)

## Курганская область
![plot for 44](./plot_44.png)

## Карачаево-Черкесская Республика
![plot for 77](./plot_77.png)

## Республика Мордовия
![plot for 68](./plot_68.png)

## Республика Алтай
![plot for 89](./plot_89.png)

## Республика Северная Осетия — Алания
![plot for 90](./plot_90.png)

## Республика Тыва
![plot for 79](./plot_79.png)

## Республика Ингушетия
![plot for 83](./plot_83.png)

## Камчатский край
![plot for 85](./plot_85.png)

## Республика Адыгея
![plot for 64](./plot_64.png)

## Севастополь
![plot for 69](./plot_69.png)

## Республика Марий Эл
![plot for 72](./plot_72.png)

## Чеченская Республика
![plot for 52](./plot_52.png)

## Магаданская область
![plot for 76](./plot_76.png)

## Еврейская автономная область
![plot for 74](./plot_74.png)

## Ненецкий автономный округ
![plot for 88](./plot_88.png)

## Чукотский автономный округ
![plot for 86](./plot_86.png)

