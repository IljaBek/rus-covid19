#!/usr/bin/env python3

import numpy as np
import pandas as pd

import matplotlib as mpl
mpl.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
plt.style.use('classic')
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


### Read region IDs, names and populations
# Source: https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/Administrativ/04-kreise.xlsx


# ks

### Assemble DataFrame

#casesMAGS = None

import json

import requests
import string
import io

# ID = 5334
# dowload file
def get_df_from_json():
  cmd = "wget 'https://coronavirus-monitor.ru/jquery-lite-9.js' -qO- | sed 's@[ ]*window.dataFromServer = @@g'  > jc.json"
  fullJ = json.load(open('jc.json'))
  jRUS = pd.io.json.json_normalize(data = fullJ['russianSubjects']['data']['subjects'], record_path='statistics', meta=['id','ru','en'] )
  jRUS['date'] = pd.to_datetime(jRUS['date'], dayfirst=True)
  jRUS.set_index(['id', 'date'], inplace=True)
  return jRUS

jRUS = get_df_from_json()
ks = jRUS.reset_index()['id'].unique()

### Data Preparation

COVIDDELAY_d = 14 # guess from data

def ser_delayed(ser, COVIDDELAY_d = 15):
  ser_del = ser.copy()
  ser_del.index = ser.index + pd.DateOffset(days=COVIDDELAY_d)
  
  set_act_del =  ser.resample('D').mean().interpolate(method='linear') - ser_del.resample('D').mean().interpolate(method='linear')
#   df_tmp = pd.concat( [pd.DataFrame( { 'x': ser}), pd.DataFrame( { 'y': set_act_del })], axis=1)
  df_tmp = pd.DataFrame( { 'x': ser}).join( pd.DataFrame( { 'y': set_act_del }))
  return df_tmp['y']


def plot_jid(casesMAGS, k):
  fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=False, num=None, figsize=(1000/80.,2*400/80. ), dpi=80, facecolor='w', edgecolor='k')
  cl = 'tab:blue'
  cl2 = 'black'
  ax, ax2 = axs
  ax.set_title(casesMAGS.loc[k]['ru'][0])
  ax.set_ylabel("... Cases", color=cl)
#   ax2 = ax.twinx()
  ax.tick_params(axis='y', labelcolor=cl)
  ax2.tick_params(axis='y', labelcolor=cl2)
  ax2.set_ylabel("Active Cases", color=cl2)
#   ax2.plot( ser_delayed( casesMAGS.loc[k]['confirmed'] , 7 ) , lw=0.8, ms=1, marker='o', label=' Confirmed - Confirmed-{:}d-earlier'.format(7))  
  ax2.plot( ser_delayed( casesMAGS.loc[k]['confirmed'] , COVIDDELAY_d ) , lw=0.8, ms=1, marker='o', label=' Confirmed - Confirmed-{:}d-earlier'.format(COVIDDELAY_d))  
  ax.plot( casesMAGS.loc[k]['confirmed'] ,lw=0.9, label='Total Confirmed')
  ax.plot( casesMAGS.loc[k]['deaths']*100. ,lw=0.9, label='Deaths x 100')
  ax.plot( (casesMAGS.loc[k]['deaths']+casesMAGS.loc[k]['cured']) ,lw=0.9, label='Cured + Deaths')
  ax2.plot( (casesMAGS.loc[k]['confirmed']-casesMAGS.loc[k]['deaths']-casesMAGS.loc[k]['cured']), label='Active = Confirmed - (Cured + Deaths)')
#   ax2.axhline(y=50, color='red', linestyle='--', lw=2, label='50 cases/100.000')
  ax.set_ylim(bottom=0)
  ax2.set_ylim(bottom=0)
  ax.yaxis.set_ticks_position('right')
  ax2.yaxis.set_ticks_position('right')
  ax.yaxis.set_label_position('right')
  ax2.yaxis.set_label_position('right')
#   ax.yaxis.tick_right()
#   ax2.yaxis.tick_right()
  leg = ax.legend(loc='upper left')
  leg2 = ax2.legend(loc='upper left')
#  ax.add_artist(leg)
#  ax2.get_legend().remove()
#  ax.add_artist(leg2)
#  leg2.loc = 'upper center'
  leg.get_frame().set_edgecolor(cl)
  leg2.get_frame().set_edgecolor(cl2)
  ax2.tick_params(axis='x', labelrotation=70)
  ax2.xaxis.set_major_formatter(mdates.DateFormatter('%d-%m-%Y'))
  # ax.xaxis.set_major_locator(mdates.DayLocator([interval=7]))
  ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=mpl.dates.MO))
  ax.xaxis.grid(True, which='major')
  ax2.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=mpl.dates.MO))
  ax2.xaxis.grid(True, which='major')
  for label in ax2.get_xticklabels():
      label.set_rotation(40)
      label.set_horizontalalignment('right')
  fig.tight_layout()
  fname = 'plot_{:}.png'.format(k)
  plt.savefig(fname)
#   plt.show()
  return fname, fig, ax, ax2

# plot_jid(jRUS, 6)
  
# fn,_,_,_ = plot_key(5334)
  
# plot_key(5334)

def plot_cum(casesMAGS, col, ylabel):
  ti = casesMAGS.loc[2].index[-1]
  ks_ord = casesMAGS.swaplevel(0,1).loc[pd.IndexSlice[ti]][col].sort_values(ascending=False).index.to_list()

  sp = casesMAGS.loc[2].shape

  Nc = sp[1]
  cm = plt.get_cmap('gist_ncar')


  fig, ax = plt.subplots(nrows=1, ncols=1, sharex=False, sharey=False, num=None, figsize=(2000/80.,1200/80. ), dpi=80, facecolor='w', edgecolor='k')


  cum = np.zeros(sp[0])
  for i,k in enumerate(ks_ord):
    c = ks_names[k]
    df = casesMAGS.loc[k][col]
    cum += df.values
    ax.fill_between(df.index, cum, cum - df.values, label='MAGS '+c , color=cm(1.*(i%10)/10+0.05))
  ax.set_ylabel(ylabel)
  # ax.set_yscale("log")
  handles, labels = ax.get_legend_handles_labels()
  #ax.legend(handles[::-1], labels[::-1], loc='center left', bbox_to_anchor=(1, 0.5))
  ax.legend(handles[::-1], labels[::-1], loc='upper center', ncol=3,handleheight=2.4, labelspacing=0.05)
  ax.tick_params(axis='x', labelrotation=70)

  # ax.annotate('iljabek@gmail.com', xy = [0.005,0.005], xycoords='axes fraction', size=15, color="#FCFCFC")

  ax.xaxis.set_major_formatter(mdates.DateFormatter('%d-%m-%Y'))
  # ax.xaxis.set_major_locator(mdates.DayLocator([interval=7]))
  ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=mpl.dates.MO))
  ax.xaxis.grid(True, which='major')
  for label in ax.get_xticklabels():
      label.set_rotation(40)
      label.set_horizontalalignment('right')
  fig.tight_layout()
  fname = 'plot_cum_{:}.png'.format(col)
  fig.savefig(fname)
#   plt.show()
  return fname, fig, ax

# fn,_,_ = plot_cum



def main():

    ### Plot Production Loop
    log = open("README.md", 'w')
    log.write("""# SARS-COV-2 Case numbers in RUS


""")

#     for P in [('confirmed', "Cumulative Cases" ), ('delayed', "Cumulative  cases - cases-{:}d-earlier".format(COVIDDELAY_d) )]:
#         fn,_,_ = plot_cum(casesMAGS, *P)
#         log.write("""## {:}
# ![plot of {:}](./{:})

# """.format(P[1], P[0], fn))
#         print(P[0], 'done!')


    for k in ks:
#    for k in [5570]:
        fn,f,_,_ = plot_jid(jRUS, k)
        plt.close(f)
        log.write("""## {:}
![plot for {:}](./{:})

""".format(jRUS.loc[k]['ru'][0], k, fn))
        print(k, 'done!')
    
    log.close()

if __name__ == '__main__':
    main()
